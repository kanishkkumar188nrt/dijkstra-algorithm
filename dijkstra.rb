class Graph
  Nexus = Struct.new(:node, :neighbours, :dist, :prev)
 
  def initialize(graph)
    @vertices = Hash.new{|h,k| h[k]=Nexus.new(k,[],Float::INFINITY)}
    @edges = {}
    graph.each do |(v1, v2, dist)|
      @vertices[v1].neighbours << v2
      @vertices[v2].neighbours << v1
      @edges[[v1, v2]] = @edges[[v2, v1]] = dist
      
    end
    @dijkstra_source = nil
  end
 
  def dijkstra(source)
    return  if @dijkstra_source == source
    q = @vertices.values
    @vertices[source].dist = 0
    until q.empty?
      u = q.min_by {|nexus| nexus.dist}
      break if u.dist == Float::INFINITY
      
      q.delete(u)
      u.neighbours.each do |v|
        vv = @vertices[v.to_sym]
        if q.include?(vv)
          alt = u.dist + @edges[[u.node, v]]
          if alt < vv.dist
            vv.dist = alt
            vv.prev = u.node
          end
        end
      end
    end
    @dijkstra_source = source
  end
 
  def shortest_path(source, target)
    dijkstra(source)
    path = []
    u = target
    while u
      path.unshift(u)
      u = @vertices[u].prev
    end
    return path, @vertices[target].dist
  end
 
end
 
g = Graph.new([ [:a,:b,8],
      [:a,:c,2],
      [:a,:d,5],
      [:b,:f,13],
      [:b,:d,2],
      [:c,:d,2],
      [:c,:e,5],
      [:d,:e,1],
      [:d,:f,6],
      [:d,:g,3],
      [:e,:g,1],
      [:f,:h,3],
      [:f,:g,2],
      [:g,:h,6]
              ])
 
start, stop = :a, :h
path, dist = g.shortest_path(start, stop)
puts "shortest path from #{start} to #{stop} has cost #{dist}:"
puts path.join(" -> ")